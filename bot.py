import sys
import telepot
import socket
import time
import random
import datetime

global ziekmeldingen
ziekmeldingen = []
global subscribers
subscribers = []
admin = [162012757, 128180967]

global targetTime
targetTime = "08:00 AM"

try:
    with open("ziekmeldingen.txt", 'r') as z:
        ziekmeldingen = z.read().split('&')
        ziekmeldingen.remove("")
except:
    pass

try:
    with open("subscribers.txt", 'r') as s:
        subscribers = s.read().split('&')
        subscribers.remove("")
except:
    pass


def saveZ():
    global ziekmeldingen
    with open("ziekmeldingen.txt", 'w') as z:
        for x in range(0, len(ziekmeldingen)):
            if x < len(ziekmeldingen):
                if len(ziekmeldingen[x]) > 1:
                    z.write(str(ziekmeldingen[x]) + "&")
            else:
                z.write(str(ziekmeldingen[x]))


def saveS():
    global subscribers
    with open("subscribers.txt", 'w') as s:
        for x in range(0, len(subscribers)):
            if x < len(subscribers):
                if len(subscribers[x]) > 1:
                    s.write(str(subscribers[x]) + "&")
            else:
                s.write(str(subscribers[x]))


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


def absenten(chat_id):
    global ziekmeldingen
    today = now.strftime("%d-%m-%Y")
    date = datetime.datetime.strptime(today, "%d-%m-%Y") + datetime.timedelta(days=1)
    string = "Absenten voor " + datetime.datetime.strftime(date, "%d-%m-%Y") + ":\n"
    ziek = ""
    for x in range(0, len(ziekmeldingen)):
        ziek += ziekmeldingen[x] + "\n"
    try:
        bot.sendMessage(chat_id, string + ziek)
    except:
        pass


def handle(msg):
    global ziekmeldingen
    global subscribers
    chat_id = msg['chat']['id']
    command = msg['text']

    if chat_id in admin:
        if command == '/ip':
            bot.sendMessage(chat_id, get_ip())

        if '/ziek' in command:
            if command.replace('/ziek', '') not in ziekmeldingen:
                ziekmeldingen.append(command.replace('/ziek', ''))
                bot.sendMessage(chat_id, command.replace('/ziek', '') + " ziekgemeld")
                saveZ()
            else:
                bot.sendMessage(chat_id, command.replace('/ziek', '') + " reeds ziekgemeld")

        if '/beter' in command:
            if command.replace('/beter', '') in ziekmeldingen:
                ziekmeldingen.remove(command.replace('/beter', ''))
                bot.sendMessage(chat_id, command.replace('/beter', '') + " betergemeld")
                saveZ()
            else:
                bot.sendMessage(chat_id, command.replace('/beter', '') + " reeds betergemeld")

        if '/notificatie_tijd' in command:
            global targetTime
            timeString = command.replace('/notificatie_tijd ', '')
            try:
                datetime.datetime.strptime(timeString, '%I:%M %p')
                targetTime = timeString
                bot.sendMessage(chat_id, "tijd doorgegeven: " + timeString)
            except ValueError:
                bot.sendMessage(chat_id, "Incorrecte tijdsnotatie! Een correcte notatie ziet eruit als: '08:15 PM'")

        if command == '/absenten':
            absenten(chat_id)

        if command == '/notificaties_aan':
            if chat_id not in subscribers:
                subscribers.append(str(chat_id))
                saveS()
            bot.sendMessage(chat_id, "notificaties aan, ingesteld op " + targetTime)

        if command == '/notificaties_uit':
            if chat_id in subscribers:
                subscribers.remove(str(chat_id))
                saveS()
            bot.sendMessage(chat_id, "notificaties uit")


bot = telepot.Bot('484097724:AAHZjC6fuEGmtA2z-klp1M-J_ywVtXeUWYo')
bot.message_loop(handle)

while 1:
    now = datetime.datetime.now()
    currentTime = now.strftime("%I:%M %p")
    if currentTime == targetTime:
        for x in range(0, len(subscribers)):
            absenten(subscribers[x])
        time.sleep(60)
    time.sleep(10)
